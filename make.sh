#!/bin/sh

FONT_ROOT_DIR=/usr/share/fonts
OTF_DIR="${FONT_ROOT_DIR}"/opentype
TTF_DIR="${FONT_ROOT_DIR}"/truetype

if [ "$(id -u)" -ne 0 ]; then
	printf 'This script must be run as root\n' >&2
	exit 1
fi

mkdir -p "${OTF_DIR}"
mkdir -p "${TTF_DIR}"

printf 'Installing fonts to %s ...\n' "${FONT_ROOT_DIR}"

ln -s "$(readlink -f helvetica)" "${OTF_DIR}"
ln -s "$(readlink -f mscorefonts)" "${TTF_DIR}"

cat << EOF
[!] Finish.
[!] Run "fc-cache -fv" to update fonts cache.
EOF
