# fonts.d -- Font directory of my own

Just download it by clicking `Download zip` in
`Clone or Download` green button.

## Extracting fonts from a Windows ISO

The fonts can also be found in a Windows ISO file.
The format of the image file containing the fonts in the ISO is
either WIM (*Windows Imaging Format*) if the ISO is downloaded online
or ESD (*Windows Electronic Software Download*) if it is built with
Windows' Media Creation Tool.
Extract the `sources/install.esd` or the `sources/install.wim` file
from the `.iso` and look for a Windows/Fonts directory within this file.
It can be extracted using 7z (in p7zip) or wimextract (in wimlib).

See an example below using 7z::

```sh
7z e Win10_1709_English_x64.iso sources/install.wim
7z e install.wim 1/Windows/{Fonts/"*".{ttf,ttc}} -ofonts/
```

## Fontconfig rules useful for MS Fonts

See [here](https://wiki.archlinux.org/index.php/Microsoft_fonts#Fontconfig_rules_useful_for_MS_Fonts).

## References:

- [Microsoft fonts](https://wiki.archlinux.org/index.php/Microsoft_fonts)
