#!/bin/sh
# rename-postscript-font.sh: Rename font file to proper Postscript fontname
# License: GPL3

usage() {
  printf 'usage: rename-postscript-font.sh /path/to/font/folder\n' >&2
  exit 1
}

if [ $# -ne 2 ]; then
  usage
fi

default_target() {
  target="$1"
  [ -z "$target" ] && target=.
  echo "$target"
}

rename_ttf_fonts() {
  TARGET=$(default_target "$1")
  for FILE in "${TARGET}"/*.ttf; do
    PS_NAME=$(fc-scan --format '%{postscriptname}\n' "${FILE}" | head -n1)
    mv "$FILE" "${TARGET}/${PS_NAME}".ttf
  done
}

rename_ttc_fonts() {
  TARGET=$(default_target "$1")
  for FILE in "${TARGET}"/*.ttc; do
    PS_NAME=$(exiftool "${FILE}" \
        | awk -v FS=: '/Post Script Font Name \(en-US\)/{print $2}' \
        | tr -d ' ')
    mv "${FILE}" "${TARGET}/${PS_NAME}".ttc
  done
}

rename_ttf_fonts "$1"
rename_ttc_fonts "$1"
